# POO. PRÁCTICA UF4
## COMPUTADORA
### Yostin Arbey Camacho Velásquez

Este proyecto se trata de representar, a través de JAVA, una computadora. Realizando  
una abstracción de la misma junto con todo lo que la compone, tal como la placa base,  
el procesador, almacenamiento, etc.  
  
El documento presente registra de forma sencilla un poco del funcionamiento, resultado   
final y algun que otro cambio que se hizo durante el desarrollo que difiere en pequeña   
medida a la visión dada por el docente. 

Así, doy comienzo con las:

## DIFERENCIAS

* **INTERPRETAR ERRORRES**  
Uno de los primeros cambios que decidí fue el de no hacer una funcion de *infoError()*   
dentro de las classes de **Computadora** y **PlacaBase**. Ya que preferí que cada una  
hiciera su trabajo correspondiente y que retornara el estado de cada operación. Lo que   
hice fue manejar un estandar en los errores y que cada método retornara el indicado para  
poder interpretarlo con *infoError()* que se encuentra en la clase con el **main()**. 

    ![infoError](mss.png)  

Dentro del proyeto tambíen hay documentación que permite ver los errores que cada  
método maneja y puede retornar
  
* **ATRIBUTOS COMPUTADORA**   
Los atributos de la classe computadora y sus tipos me dieron mucho tiempo para pensar.  
Así que decidí cambiarlo un poco para facilitarme el trabajo. Originalmente muchos  
de estos eran un **boolean** que hacían referencia a si un dispositivo estaba conectado  
o no; el cambio que realizé fue cambiar el tipo al de la clase al que cada dispositivo  
correspondía.ç
![atributo](atributos.png)  


Estas son las diferencias más significativas, el resto son modificaciones simples como   
cambiar el mombre de un atributo o el tipo de retorno de un método.

## FUNCIONAMIENTO
El único método del que me interesa hablar de su funcionamiento es **conectarDispositivo()**  
dela clase Computadora. Ya que creo que el espaueti resuntante de hacer tantas   
comprobaciones y las expresiones utilizadas pueden ser confusas.  
  
Escencialmemte el método hace un tipo de "contrato" en el que antes de comenzar a  
mirar qué tipo de dispositivo entra, comprueba:  
1. Si el dispositivo es igual a **null**
2. Si la tapa de la computadora está abierta para poder continuar
3. Si ya hay una placa base donde conectar los dispositivos
4. Y si estoy intentando conectar un dispositivo que ya lo estaba

![poli](poli.png)

## RESULTADO FINAL  
Aquí, una la salida por pantalla luego de realizar las correspondientes instancias  
e instalaciones correspondientes dentro de la computadora. 
 
![fin](final.png)  
![fin1](final1.png)
---
Preparando la computadora...


Preparando la placa base...


Instalando el Microprocesador en la placa base...  
Dispositivo instalado correctamente...


Instalando la memoria RAM en la placa base...  
Dispositivo instalado correctamente...


Instalando la targeta gráfica en la placa base...  
Dispositivo instalado correctamente...


Instalando la PlacaBase en la computadora...  
Dispositivo instalado correctamente...


Conectando un disco duro a la computadora...  
Dispositivo instalado correctamente...


Conectando un teclado al computador...  
Dispositivo instalado correctamente...


Conectando un monitor al computador...  
Dispositivo instalado correctamente...


Conectando un ratón al computador...  
Dispositivo instalado correctamente...


Conectando un DVD a la computadora...  
Dispositivo instalado correctamente...


Conectando una fuente de alimentación a la computadora...  
Dispositivo instalado correctamente...


Iniciando computadora...  
PC iniciada...

